import React from 'react';
import {useForm,  useFormContext, useFieldArray} from 'react-hook-form';

function Chapter({chapter, index: paretInd}) {
    console.log('chapter', chapter)

    const {register, control} = useFormContext();

    const { fields, append } = useFieldArray({
        control,
        name: 'sections'
    });
 
    return (
        <>
            <input name={`chapters[${paretInd}].title`} placeholder="title" defaultValue={chapter.title} ref={register()} />
            {fields.map( (section, index) => (
                <div key={section.id}>
                    <input name={`chapters[${paretInd}].sections[${index}].title`} ref={register()} placeholder="section title" defaultValue={section.title} />
                    
                </div>
            ))}
            <button onClick={() => append({title: ''})}>Add Section</button>
        </>
    );
}

export default React.memo(Chapter);