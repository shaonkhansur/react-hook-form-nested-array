import * as React from "react";
import { useForm, FormProvider, useFieldArray } from "react-hook-form";
import Chapter from './Chapter';

const App = () => {
  const methods = useForm({
    defaultValues: {
      courseName: 'Physic',
      organizationName: '',
      courseNumber: '',
      courseRun: '',
      chapters: [{title: 'chapter one'}],
    }
  });
  const {control, register, handleSubmit} = methods;
  const {fields, append, remove} = useFieldArray({
    control,
    name: 'chapters'
  })

  const onSubmit = data => console.log(data);

  return (
    <>
      <FormProvider {...methods}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <input name="courseName" ref={register} placeholder="Course Name" defaultValue="courseName"/>
          <input name="organizationName" ref={register} placeholder="Organization Name" defaultValue="organizationName"/>
          <input name="courseNumber" ref={register} placeholder="Course Number" defaultValue="courseNumber"/>
          <input name="courseRun" ref={register} placeholder="Course Run" defaultValue="courseRun"/>
          {fields.map( (chapter, index) => (
            <div key={chapter.id}>
              <Chapter chapter={chapter} index={index} />              
            </div>
          ))}
          <button onClick={() => append({title: '', sections: []})}>Add Chapter</button>
          <button type="submit">Submit</button>
        </form>
      </FormProvider>
    </>
  );
};

export default App;